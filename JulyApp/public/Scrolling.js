﻿$(function () {
    $(window)
        .on("load resize",
        function () {
            $(".fill-screen").css("height", window.innerHeight);
        });
    $("#video-background").wallpaper({
        source: {
            poster: "images/BackgroundImgMobile.jpg",
            mp4: "videos/myClip2.mp4"
        }
    });
});